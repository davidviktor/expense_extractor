﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpenseExtractor.BusinessLogic.Interfaces
{
    public interface IXmlParsingManager
    {
        Dictionary<string, string> ParseTextToTags(string inputText);
    }
}
