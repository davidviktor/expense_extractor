﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpenseExtractor.BusinessLogic.Interfaces
{
    public interface IGstManager
    {
        decimal ComputeGst(decimal gstBase);
    }
}
