﻿using ExpenseExtractor.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExpenseExtractor.BusinessLogic.Interfaces
{
    public interface IExpenseEmailManager
    {
        ExtractedExpense ExtractExpense(Email email);
    }
}
