﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpenseExtractor.BusinessLogic.Constants
{
    public class XmlTags
    {
        public const string Total = "total";

        public const string CostCentre = "cost_centre";
    }
}
