﻿using ExpenseExtractor.BusinessLogic.Interfaces;
using ExpenseExtractor.Models.Exceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;

namespace ExpenseExtractor.BusinessLogic.Managers
{
    public class XmlParsingManager : IXmlParsingManager
    {
        private readonly ILogger<XmlParsingManager> _logger;

        public XmlParsingManager(ILogger<XmlParsingManager> logger)
        {
            _logger =
                logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public Dictionary<string, string> ParseTextToTags(string inputText)
        {
            _logger.LogInformation($"Parsing xml for input text {inputText}");

            var result = new Dictionary<string, string>();

            if (inputText == null)
            {
                _logger.LogInformation($"No input text provided.");

                return result;
            }

            IterateOverAndParseXmlElementsFromText(inputText, result);

            _logger.LogInformation($"Finished parsing xml for input text {inputText}");

            return result;
        }

        private void IterateOverAndParseXmlElementsFromText(string inputText, Dictionary<string, string> result)
        {
            var xmlElementNamePattern = @"<[\w-.]+>";
            var closedXmlElementPattern = @"(?<open><(?<openname>[\w-.]+)>).*?(?<close-open></\k<openname>>)";
            var remainingInputText = inputText;
            var xmlElementOpeningTagMatch = Regex.Match(remainingInputText, xmlElementNamePattern);

            while (xmlElementOpeningTagMatch.Success)
            {
                _logger.LogInformation($"Found xml opening tag {xmlElementOpeningTagMatch.Value}.");

                var closedXmlElementMatch = Regex.Match(remainingInputText, closedXmlElementPattern);

                if (!closedXmlElementMatch.Success)
                {
                    _logger.LogError($"No closing tag for {xmlElementOpeningTagMatch.Value}.");

                    throw new InvalidXmlContentException();
                }

                XElement parsedXmlElement = ParseMatchToXmlElement(closedXmlElementMatch);

                var leafXmlElements = FilterLeafElements(parsedXmlElement);

                AddXmlElementsToResult(result, leafXmlElements);

                remainingInputText = remainingInputText.Substring(closedXmlElementMatch.Index + closedXmlElementMatch.Length);
                xmlElementOpeningTagMatch = Regex.Match(remainingInputText, xmlElementNamePattern);
            }
        }

        private void AddXmlElementsToResult(Dictionary<string, string> result, List<XElement> leafXmlElements)
        {
            foreach (var xmlElement in leafXmlElements)
            {
                if (result.ContainsKey(xmlElement.Name.LocalName))
                {
                    _logger.LogError($"Duplicate xml tag {xmlElement.Name.LocalName}.");

                    throw new MultipleEqualXmlTagsException($"Duplicate xml tag {xmlElement.Name.LocalName}");
                }

                _logger.LogInformation($"Found xml element {xmlElement.Name.LocalName} with value {xmlElement.Value}.");

                result.Add(xmlElement.Name.LocalName, xmlElement.Value);
            }
        }

        private XElement ParseMatchToXmlElement(Match closedXmlElementMatch)
        {
            XElement parsedXmlElement = null;
            try
            {
                parsedXmlElement = XElement.Parse(closedXmlElementMatch.Value);
            }
            catch (XmlException exception)
            {
                _logger.LogError($"Error parsing xml.", exception);

                throw new InvalidXmlContentException();
            }

            return parsedXmlElement;
        }

        private List<XElement> FilterLeafElements(XElement parsedXmlElement)
        {
            List<XElement> leafElements = new List<XElement>();

            _logger.LogInformation($"Filtering leaf elements for xml element {parsedXmlElement.Name.LocalName}.");

            if (parsedXmlElement.HasElements)
            {
                var parentElements = new Queue<XElement>();
                parentElements.Enqueue(parsedXmlElement);
                while (parentElements.Count > 0)
                {
                    var nextCheckedElement = parentElements.Dequeue();

                    _logger.LogInformation($"Processing element {nextCheckedElement.Name.LocalName}.");

                    if (nextCheckedElement.HasElements)
                    {
                        _logger.LogInformation($"Processing element {nextCheckedElement.Name.LocalName} queueing child elements.");

                        nextCheckedElement.Elements().ToList().ForEach(x => parentElements.Enqueue(x));
                    }
                    else
                    {
                        _logger.LogInformation($"Saving leaf element {nextCheckedElement.Name.LocalName}.");

                        leafElements.Add(nextCheckedElement);
                    }
                }
            }
            else
            {
                _logger.LogInformation($"Saving leaf element {parsedXmlElement.Name.LocalName}.");

                leafElements.Add(parsedXmlElement);
            }

            _logger.LogInformation($"Finished filtering leaf elements for xml element {parsedXmlElement.Name.LocalName}.");

            return leafElements;
        }
    }
}
