﻿using ExpenseExtractor.BusinessLogic.Constants;
using ExpenseExtractor.BusinessLogic.Interfaces;
using ExpenseExtractor.Models;
using ExpenseExtractor.Models.Exceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ExpenseExtractor.BusinessLogic.Managers
{
    public class ExpenseEmailManager : IExpenseEmailManager
    {
        private readonly IXmlParsingManager _xmlParsingManager;

        private readonly IGstManager _gstManager;

        ILogger<ExpenseEmailManager> _logger;

        public ExpenseEmailManager(IXmlParsingManager xmlParsingManager, IGstManager gstManager, ILogger<ExpenseEmailManager> logger)
        {
            _xmlParsingManager =
                xmlParsingManager ?? throw new ArgumentNullException(nameof(xmlParsingManager));
            _gstManager =
                gstManager ?? throw new ArgumentNullException(nameof(gstManager));
            _logger =
                logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public ExtractedExpense ExtractExpense(Email email)
        {
            _logger.LogInformation("Processing email.");

            if (email == null)
            {
                _logger.LogError("There is no email");

                throw new NoEmailException();
            }

            try
            {
                var parsedXmlTags = 
                    _xmlParsingManager.ParseTextToTags(email.RawContent);

                if (parsedXmlTags == null
                    || !parsedXmlTags.ContainsKey(XmlTags.Total)
                    || !decimal.TryParse(parsedXmlTags[XmlTags.Total], NumberStyles.Number, CultureInfo.InvariantCulture, out decimal total))
                {
                    _logger.LogError("Missing total tag or value not number.");

                    throw new EmailRejectedException($"Missing {XmlTags.Total} element.");
                }

                var costCentre =
                    parsedXmlTags.ContainsKey(XmlTags.CostCentre)
                        ? parsedXmlTags[XmlTags.CostCentre]
                        : ExpenseDefaults.CostCentre;

                _logger.LogInformation("Computing GST.");

                var gst = _gstManager.ComputeGst(total);

                _logger.LogInformation("Finished processing email.");

                return new ExtractedExpense
                {
                    Total = total,
                    Gst = gst,
                    TotalWithoutGst = total - gst,
                    CostCentre = costCentre,
                    Message =
                        costCentre == ExpenseDefaults.CostCentre
                            ? "Missing cost centre" 
                            : string.Empty
                };
            }
            catch (InvalidXmlContentException exception)
            {
                _logger.LogError("Invalid xml in email.", exception);

                throw new EmailRejectedException(
                    "Invalid xml. Please make sure all tags have a closing tag.");
            }
            catch (MultipleEqualXmlTagsException exception)
            {
                _logger.LogError($"Invalid xml in email: multiple identical tags. {exception.Message}.", exception);

                throw new EmailRejectedException(
                    "Invalid xml. Please make sure there is only 1 expense is reported.");
            }
        }
    }
}
