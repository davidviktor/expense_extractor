﻿using ExpenseExtractor.BusinessLogic.Constants;
using ExpenseExtractor.BusinessLogic.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExpenseExtractor.BusinessLogic.Managers
{
    public class GstManager : IGstManager
    {
        private decimal GstRate { get; set; }

        private readonly ILogger<GstManager> _logger;

        public GstManager(decimal gstRate, ILogger<GstManager> logger)
        {
            GstRate = gstRate;
            _logger =
                logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public decimal ComputeGst(decimal gstBase)
        {
            _logger.LogInformation($"Computing GST for value {gstBase}");

            var computedGst = gstBase * GstRate;

            _logger.LogInformation($"Computed GST for value {gstBase} is {computedGst}.");

            return computedGst;
        }
    }
}
