using ExpenseExtractor.BusinessLogic.Interfaces;
using ExpenseExtractor.Controllers;
using ExpenseExtractor.Models;
using ExpenseExtractor.Models.Exceptions;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        ExpenseEmailController _expenseEmailController;

        Mock<IExpenseEmailManager> _expenseEmailManager;
        Mock<ILogger<ExpenseEmailController>> _logger;

        [Test]
        public void When_Post_Then_Manager_Is_Called()
        {
            var email = new Email();

            _expenseEmailController.Post(email);

            _expenseEmailManager.Verify(
                e => e.ExtractExpense(email), 
                Times.Once,
                "Manager not called as expected!");
        }

        [Test]
        public void When_Post_Then_Manager_Result_Is_Returned()
        {
            var email = new Email();
            var extractedExpense = new ExtractedExpense();
            _expenseEmailManager
                .Setup(e => e.ExtractExpense(email))
                .Returns(extractedExpense);

            var result = _expenseEmailController.Post(email);

            Assert.IsNotNull(result, "No response!");
            Assert.AreEqual(extractedExpense, result.Value, "Returned result not as expected!");
        }

        [Test]
        public void When_Post_Fail_Then_UserException_Is_Thrown()
        {
            var email = new Email();
            var expectedException = "test exception";
            var extractedExpense = new ExtractedExpense();
            _expenseEmailManager
                .Setup(e => e.ExtractExpense(email))
                .Throws(new EmailRejectedException(expectedException));

            Assert.Throws<UserExceptionWithFeedback>(() => _expenseEmailController.Post(email));
        }

        [SetUp]
        public void Setup()
        {
            _expenseEmailManager = new Mock<IExpenseEmailManager>();
            _logger = new Mock<ILogger<ExpenseEmailController>>();

            _expenseEmailController = new ExpenseEmailController(_expenseEmailManager.Object, _logger.Object);
        }
    }
}