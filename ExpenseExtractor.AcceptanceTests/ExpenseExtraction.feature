﻿Feature: ExpenseExtraction
	In order to be able to extract cost from emails
	As an expense officer
	I want to be able to have emails processed and get back structured result
  
Scenario: Invalid xml content should be rejected
	Given I have an email with xml as follows "Dear Jane, sorry to <send> you this invalid xml"
	When I send the email to be processed
	Then the result should be rejected

Scenario: Valid xml content with missing total tag should be rejected
	Given I have an email with xml as follows "Dear John, unfortunately this email has valid xml but no total tag <expense><cost_centre>DEV002</cost_centre></expense>"
	When I send the email to be processed
	Then the result should be rejected

Scenario: Valid xml content with missing cost_centre tag should be accepted and cost centre set to 'UNKNOWN'
	Given I have an email with xml as follows "Dear Claire, I forgot about the cost centre <expense><total>1024.01</total><payment_method>personal card</payment_method></expense>"
	When I send the email to be processed
	Then the result should be accepted with cost_center set to UNKNOWN

Scenario: Valid xml content should be accepted
	Given I have an email with xml as follows "Dear Harry, all fine here <expense><cost_centre>DEV002</cost_centre><total>1024.01</total><payment_method>personal card</payment_method></expense>"
	When I send the email to be processed
	Then the result should be accepted
