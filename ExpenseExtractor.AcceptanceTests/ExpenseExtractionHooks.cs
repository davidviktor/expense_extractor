﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Reflection;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace ExpenseExtractor.AcceptanceTests
{
    [Binding]
    public sealed class ExpenseExtractionHooks
    {
        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            RunDockerCommand("run_container.ps1");
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            RunDockerCommand("stop_container.ps1");
        }

        private static void RunDockerCommand(string filename)
        {
            int errorLevel;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("powershell.exe",
                $"-NoProfile -ExecutionPolicy Unrestricted -File {Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), filename)} -Verb runas");
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            processInfo.RedirectStandardOutput = true;
            processInfo.RedirectStandardError = true;

            process = Process.Start(processInfo);
            process.WaitForExit();

            string stdoutx = process.StandardOutput.ReadToEnd();
            string stderrx = process.StandardError.ReadToEnd();

            errorLevel = process.ExitCode;
            process.Close();
        }
    }
}
