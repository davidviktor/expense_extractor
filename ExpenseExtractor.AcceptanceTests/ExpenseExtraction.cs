﻿using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using TechTalk.SpecFlow;

namespace ExpenseExtractor.AcceptanceTests
{
    [Binding]
    public sealed class ExpenseExtraction
    {
        private string _email;
        private HttpResponseMessage _response;

        [Given(@"I have an email with xml as follows ""(.*)""")]
        public void GivenIHaveAnEmailWithXmlAsFollows(string email)
        {
            _email = email;
        }

        [When(@"I send the email to be processed")]
        public void WhenISendTheEmailToBeProcessed()
        {
            HttpClient client = new HttpClient();

            var json = 
                JsonConvert.SerializeObject(
                    new { rawContent = _email }, 
                    Formatting.Indented);

            var content = new StringContent(json, Encoding.UTF8, "application/json");
            _response = client.PostAsync("http://localhost:8080/ExpenseEmail", content).Result;
        }

        [Then(@"the result should be (accepted|rejected)(.*)?")]
        public void ThenTheResultShouldBeAccepted(string acceptedOrRejected, string condition)
        {
            if (acceptedOrRejected == "accepted")
                Assert.AreEqual(HttpStatusCode.OK, _response.StatusCode);
            else
                Assert.AreEqual(HttpStatusCode.BadRequest, _response.StatusCode);

            if (condition == " with cost_center set to UNKNOWN")
            {
                var responseResultTask = _response.Content.ReadAsStringAsync();
                responseResultTask.Wait();
                var responseContent = responseResultTask.Result;
                var responseObject = 
                    JsonConvert.DeserializeObject<ResponseMessage>(responseContent);
                Assert.AreEqual("UNKNOWN", responseObject.CostCentre);
            }
        }
    }

    public class ResponseMessage
    {
        public string CostCentre { get; set; }
    }
}
