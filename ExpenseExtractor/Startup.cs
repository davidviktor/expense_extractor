﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ExpenseExtractor.BusinessLogic.Constants;
using ExpenseExtractor.BusinessLogic.Interfaces;
using ExpenseExtractor.BusinessLogic.Managers;
using ExpenseExtractor.Models.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;

namespace ExpenseExtractor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Expense API", Version = "v1" });
            });

            services.AddScoped<IExpenseEmailManager, ExpenseEmailManager>();
            services.AddScoped<IGstManager, GstManager>(serviceProvider => 
                new GstManager(
                    Configuration.GetValue<decimal>(AppConfigKeys.GstRate), 
                    serviceProvider.GetService<ILogger<GstManager>>()));
            services.AddScoped<IXmlParsingManager, XmlParsingManager>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            loggerFactory.AddSerilog();

            app.UseExceptionHandler(appBuilder => {
                appBuilder.Run(async context =>
                {
                    HttpStatusCode responseStatusCode;
                    string responseMessage;

                    var error = context.Features.Get<IExceptionHandlerFeature>();
                    Exception exception;
                    if (error != null
                        && (exception = error.Error)?.GetType() == typeof(UserExceptionWithFeedback))
                    {
                        responseStatusCode = HttpStatusCode.BadRequest;
                        responseMessage = exception.Message;
                    }
                    else
                    {
                        responseStatusCode = HttpStatusCode.InternalServerError;
                        responseMessage =
                                "Sorry, we are experiencing an error. Please try again or in case of repeated failure, contact system administrator.";
                    }

                    context.Response.StatusCode = (int)responseStatusCode;
                    context.Response.ContentType = "application/json";

                    await context.Response.WriteAsync(
                        JsonConvert.SerializeObject(
                        new
                        {
                            Message = responseMessage,
                            StatusCode = responseStatusCode
                        })).ConfigureAwait(false);
                });
            });

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Expense API V1");
            });
        }
    }
}
