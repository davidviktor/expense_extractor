﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExpenseExtractor.BusinessLogic.Interfaces;
using ExpenseExtractor.Models;
using ExpenseExtractor.Models.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ExpenseExtractor.Controllers
{
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    public class ExpenseEmailController : ControllerBase
    {
        private readonly IExpenseEmailManager _expenseEmailManager;

        private readonly ILogger<ExpenseEmailController> _logger;

        public ExpenseEmailController(IExpenseEmailManager expenseEmailManager, ILogger<ExpenseEmailController> logger)
        {
            _expenseEmailManager =
                expenseEmailManager ?? throw new ArgumentNullException(nameof(expenseEmailManager));
            _logger =
                logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public ActionResult<ExtractedExpense> Post([FromBody] Email email)
        {
            try
            {
                _logger.LogInformation("Extract expenses request received!");

                return _expenseEmailManager.ExtractExpense(email);
            }
            catch (EmailRejectedException exception)
            {
                _logger.LogError("Extract expenses request failed!", exception);

                throw new UserExceptionWithFeedback(exception.Message);
            }
        }
    }
}
