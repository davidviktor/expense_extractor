using ExpenseExtractor.BusinessLogic.Managers;
using ExpenseExtractor.Models;
using NUnit.Framework;
using System.Collections.Generic;
using ExpenseExtractor.BusinessLogic.Constants;
using ExpenseExtractor.BusinessLogic.Interfaces;
using Moq;
using ExpenseExtractor.Models.Exceptions;
using Microsoft.Extensions.Logging;
using System.Globalization;

namespace Tests
{
    public class Tests
    {
        ExpenseEmailManager _expenseEmailManager;

        Mock<IXmlParsingManager> _xmlParsingManager;
        Mock<IGstManager> _gstManager;
        Mock<ILogger<ExpenseEmailManager>> _logger;

        [Test]
        public void When_Null_Email_Then_Exception_Is_Thrown()
        {
            Assert.Throws<NoEmailException>(() => _expenseEmailManager.ExtractExpense(null));
        }

        [Test]
        public void When_Invalid_Xml_Then_Exception_Is_Thrown()
        {
            var email = new Email { RawContent = "test" };
            _xmlParsingManager
                .Setup(x => x.ParseTextToTags(It.IsAny<string>()))
                .Throws<InvalidXmlContentException>();

            Assert.Throws<EmailRejectedException>(() => _expenseEmailManager.ExtractExpense(email));
        }

        [Test]
        public void When_Missing_Required_Tag_Then_Exception_Is_Thrown()
        {
            var email = new Email { RawContent = "test" };
            _xmlParsingManager
                .Setup(x => x.ParseTextToTags(It.IsAny<string>()))
                .Returns(new Dictionary<string, string>());

            Assert.Throws<EmailRejectedException>(() => _expenseEmailManager.ExtractExpense(email));
        }

        [Test]
        public void When_Null_Result_From_Parser_Then_Exception_Is_Thrown()
        {
            var email = new Email { RawContent = "test" };
            _xmlParsingManager
                .Setup(x => x.ParseTextToTags(It.IsAny<string>()))
                .Returns(() => null);

            Assert.Throws<EmailRejectedException>(() => _expenseEmailManager.ExtractExpense(email));
        }

        [Test]
        public void When_Email_As_Excpected_Then_Data_Is_Extracted()
        {
            var email = new Email { RawContent = "test" };
            var expectedTotal = 120.0m;
            var expectedGst = 18.0m;
            var expectedTotalWithoutGst = expectedTotal - expectedGst;
            var expectedCostCentre = "test centre";
            var expectedTags =
                new Dictionary<string, string>
                {
                    { XmlTags.Total, expectedTotal.ToString(CultureInfo.InvariantCulture) },
                    { XmlTags.CostCentre, expectedCostCentre },
                };
            _xmlParsingManager
                .Setup(x => x.ParseTextToTags(It.IsAny<string>()))
                .Returns(expectedTags);
            _gstManager
                .Setup(g => g.ComputeGst(It.IsAny<decimal>()))
                .Returns((decimal total) => total * 0.15m);

            var extractedData = _expenseEmailManager.ExtractExpense(email);

            _xmlParsingManager.Verify(x => x.ParseTextToTags(It.IsAny<string>()), Times.Once);
            _gstManager.Verify(g => g.ComputeGst(It.IsAny<decimal>()), Times.Once);
            Assert.IsNotNull(extractedData, "No data extracted!");
            Assert.AreEqual(expectedTotal, extractedData.Total, "Total is not as expected!");
            Assert.AreEqual(expectedGst, extractedData.Gst, "GST is not as expected!");
            Assert.AreEqual(expectedTotalWithoutGst, extractedData.TotalWithoutGst, "Total without GST is not as expected!");
            Assert.AreEqual(expectedCostCentre, extractedData.CostCentre, "Cost centre is not as expected!");
        }

        [Test]
        public void When_Cost_Centre_Missing_Then_Set_To_Unknown()
        {
            var email = new Email { RawContent = "test" };
            var expectedTotal = 120.0m;
            var expectedGst = 18.0m;
            var expectedTotalWithoutGst = expectedTotal - expectedGst;
            var expectedTags =
                new Dictionary<string, string>
                {
                    { XmlTags.Total, expectedTotal.ToString() }
                };
            _xmlParsingManager
                .Setup(x => x.ParseTextToTags(It.IsAny<string>()))
                .Returns(expectedTags);

            var extractedData = _expenseEmailManager.ExtractExpense(email);

            Assert.AreEqual(ExpenseDefaults.CostCentre, extractedData.CostCentre, "Cost centre is not as expected!");
        }

        [SetUp]
        public void Setup()
        {
            _xmlParsingManager = new Mock<IXmlParsingManager>();
            _gstManager = new Mock<IGstManager>();
            _logger = new Mock<ILogger<ExpenseEmailManager>>();

            _expenseEmailManager = 
                new ExpenseEmailManager(_xmlParsingManager.Object, _gstManager.Object, _logger.Object);
        }
    }
}