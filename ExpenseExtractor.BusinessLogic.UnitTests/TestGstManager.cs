﻿using ExpenseExtractor.BusinessLogic.Constants;
using ExpenseExtractor.BusinessLogic.Interfaces;
using ExpenseExtractor.BusinessLogic.Managers;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExpenseExtractor.BusinessLogic.UnitTests
{
    public class TestGstManager
    {
        GstManager _gstManager;

        Mock<ILogger<GstManager>> _logger;

        [TestCase(100, 15)]
        [TestCase(0, 0)]
        [TestCase(50, 7.5)]
        [TestCase(1250, 187.5)]
        public void When_ComputeGst_Then_Expected_Value_Is_Returned(
            decimal gstBase, decimal expectedGstValue)
        {
            var gst = _gstManager.ComputeGst(gstBase);

            Assert.AreEqual(expectedGstValue, gst, "GST is not as expected!");
        }

        [SetUp]
        public void Setup()
        {
            _logger = new Mock<ILogger<GstManager>>();

            _gstManager = new GstManager(0.15m, _logger.Object);
        }
    }
}
