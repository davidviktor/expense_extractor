﻿using ExpenseExtractor.BusinessLogic.Managers;
using ExpenseExtractor.Models.Exceptions;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpenseExtractor.BusinessLogic.UnitTests
{
    public class TestXmlParsingManager
    {
        XmlParsingManager _xmlParsingManager;

        Mock<ILogger<XmlParsingManager>> _logger;

        [TestCase("text <tag>")]
        [TestCase("text </tag><tag>")]
        [TestCase("text <tag1><tag2></tag1></tag2>")]
        [TestCase("text <tag></anothertag>")]
        public void When_ParseTextToTags_Called_With_No_Closing_Tag_Then_Exception_Is_Thrown(
            string inputText)
        {
            Assert.Throws<InvalidXmlContentException>(() => _xmlParsingManager.ParseTextToTags(inputText));
        }

        [Test]
        public void When_ParseTextToTags_Called_With_Null_Then_No_Exception_Is_Thrown()
        {
            Assert.DoesNotThrow(() => _xmlParsingManager.ParseTextToTags(null));
        }

        [Test]
        public void When_ParseTextToTags_Called_With_Empty_Text_Then_No_Exception_Is_Thrown()
        {
            Assert.DoesNotThrow(() => _xmlParsingManager.ParseTextToTags(string.Empty));
        }

        [Test]
        public void When_ParseTextToTags_Called_With_Plain_Text_Without_Xml_Content_Then_No_Exception_Is_Thrown()
        {
            var inputTextWithoutXmlContent = "test plain text";
            Assert.DoesNotThrow(() => _xmlParsingManager.ParseTextToTags(inputTextWithoutXmlContent));
        }

        [Test]
        public void When_ParseTextToTags_Called_With_Text_Including_Single_Inline_Xml_Content_Then_Result_Is_As_Expected()
        {
            var expectedXmlElement = "testElement";
            var inputTextWithXmlContent = $"test text with xmlElement <{expectedXmlElement}>content</{expectedXmlElement}> inline";

            var result = _xmlParsingManager.ParseTextToTags(inputTextWithXmlContent);

            Assert.IsNotNull(result, "No result!");
            Assert.AreEqual(1, result.Keys.Count, "Number of elements not as expected!");
            Assert.IsTrue(result.ContainsKey(expectedXmlElement), "Expected element is missing!");
        }

        [Test]
        public void When_ParseTextToTags_Called_With_Text_Containing_Multiple_Inline_Xml_Content_Separated_Then_Result_Is_As_Expected()
        {
            var expectedXmlElements = new[] {
                    (TagName : "testElement1", Value : "testValue1"),
                    (TagName : "testElement2", Value : "testValue2"),
                    (TagName : "testElement3", Value : "testValue3")
                };
            var inputTextWithXmlContent = 
                $"test text with xmlElement";
            foreach (var expectedElement in expectedXmlElements)
            {
                inputTextWithXmlContent += 
                    $" <{expectedElement.TagName}>{expectedElement.Value}</{expectedElement.TagName}> text between tags";
            }

            var result = _xmlParsingManager.ParseTextToTags(inputTextWithXmlContent);

            Assert.IsNotNull(result, "No result!");
            Assert.AreEqual(expectedXmlElements.Length, result.Keys.Count, "Number of elements not as expected!");
            Assert.IsTrue(
                expectedXmlElements.All(e => result.ContainsKey(e.TagName) && result[e.TagName] == e.Value)
                && result.All(r => expectedXmlElements.Any(e => e.TagName == r.Key && e.Value == r.Value)), 
                "Expected element is missing!");
        }

        [Test]
        public void When_ParseTextToTags_Called_With_Text_Containing_Multiple_Inline_Xml_Content_Continuously_Then_Result_Is_As_Expected()
        {
            var expectedXmlElements = new[] {
                    (TagName : "testElement1", Value : "testValue1"),
                    (TagName : "testElement2", Value : "testValue2"),
                    (TagName : "testElement3", Value : "testValue3")
                };
            var inputTextWithXmlContent =
                $"test text with xmlElement ";
            foreach (var expectedElement in expectedXmlElements)
            {
                inputTextWithXmlContent += $"<{expectedElement.TagName}>{expectedElement.Value}</{expectedElement.TagName}>";
            }

            var result = _xmlParsingManager.ParseTextToTags(inputTextWithXmlContent);

            Assert.IsNotNull(result, "No result!");
            Assert.AreEqual(expectedXmlElements.Length, result.Keys.Count, "Number of elements not as expected!");
            Assert.IsTrue(
                expectedXmlElements.All(e => result.ContainsKey(e.TagName) && result[e.TagName] == e.Value)
                && result.All(r => expectedXmlElements.Any(e => e.TagName == r.Key && e.Value == r.Value)),
                "Expected element is missing!");
        }

        [Test]
        public void When_ParseTextToTags_Called_With_Text_Containing_Multiple_Of_Same_Tag_Then_Exception_Is_Thrown()
        {
            var expectedXmlElements = new[] {
                    (TagName : "testElement1", Value : "testValue1"),
                    (TagName : "testElement1", Value : "testValue2")
                };
            var inputTextWithXmlContent =
                $"test text with xmlElement ";
            foreach (var expectedElement in expectedXmlElements)
            {
                inputTextWithXmlContent += $"<{expectedElement.TagName}>{expectedElement.Value}</{expectedElement.TagName}>";
            }

            Assert.Throws<MultipleEqualXmlTagsException>(() => _xmlParsingManager.ParseTextToTags(inputTextWithXmlContent));
        }

        [Test]
        public void When_ParseTextToTags_Called_With_Text_Containing_Hierarchical_Xml_Content_Then_Result_Is_As_Expected()
        {
            var expectedXmlElements = new[] {
                    (TagName : "testElement1", Value : "testValue1"),
                    (TagName : "testElement2", Value : "testValue2"),
                    (TagName : "testElement3", Value : "testValue3")
                };
            var inputTextWithXmlContent =
                $"test text with xmlElement" +
                $"<parentElement>" +
                $"<subParentElement>";
            foreach (var expectedElement in expectedXmlElements)
            {
                inputTextWithXmlContent += $"<{expectedElement.TagName}>{expectedElement.Value}</{expectedElement.TagName}>";
            }
            inputTextWithXmlContent +=
                $"</subParentElement>" +
                $"</parentElement>";

            var result = _xmlParsingManager.ParseTextToTags(inputTextWithXmlContent);

            Assert.IsNotNull(result, "No result!");
            Assert.AreEqual(expectedXmlElements.Length, result.Keys.Count, "Number of elements not as expected!");
            Assert.IsTrue(
                expectedXmlElements.All(e => result.ContainsKey(e.TagName) && result[e.TagName] == e.Value)
                && result.All(r => expectedXmlElements.Any(e => e.TagName == r.Key && e.Value == r.Value)),
                "Expected element is missing!");
        }

        [SetUp]
        public void SetUp()
        {
            _logger = new Mock<ILogger<XmlParsingManager>>();

            _xmlParsingManager = new XmlParsingManager(_logger.Object);
        }
    }
}
