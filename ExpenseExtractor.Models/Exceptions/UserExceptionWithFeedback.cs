﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpenseExtractor.Models.Exceptions
{
    public class UserExceptionWithFeedback : ApplicationException
    {
        public UserExceptionWithFeedback(string message) : base(message)
        {

        }
    }
}
