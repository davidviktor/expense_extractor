﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpenseExtractor.Models.Exceptions
{
    public class MultipleEqualXmlTagsException : ApplicationException
    {
        public MultipleEqualXmlTagsException(string message) : base(message)
        {

        }
    }
}
