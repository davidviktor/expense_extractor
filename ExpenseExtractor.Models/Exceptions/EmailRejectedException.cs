﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpenseExtractor.Models.Exceptions
{
    public class EmailRejectedException : ApplicationException
    {
        public EmailRejectedException(string message) : base(message)
        {
        }
    }
}
