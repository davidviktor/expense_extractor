﻿using System;

namespace ExpenseExtractor.Models
{
    public class Email
    {
        public string RawContent { get; set; }
    }
}
