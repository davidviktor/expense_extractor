﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpenseExtractor.Models
{
    public class ExtractedExpense
    {
        public decimal Total { get; set; }
        public decimal Gst { get; set; }
        public decimal TotalWithoutGst { get; set; }
        public string CostCentre { get; set; }
        public string Message { get; set; }
    }
}
